const Hoek = require('hoek');
const redis = require('redis');
const BPromise = require('bluebird');
BPromise.promisifyAll(redis.RedisClient.prototype);
BPromise.promisifyAll(redis.Multi.prototype);

class Redis {

  constructor(config, log) {
    Hoek.assert(config, 'Cache redis cannot empty config');
    if (config.expire) {
      this.expire = config.expire;
      delete config.expire;
    }
    config = Hoek.applyToDefaults({
      host: 'localhost',
      port: 6379,
      prefix: 'nhutdev_'
    }, config);
    this._redisClient = redis.createClient(config);
    this._log = log;
    this._redisClient.on('error', (err) => {
      this._log.error(`Create redis cache error with ${config.host} ${config.port}`);
      this._log.error(err);
    });

    this._redisClient.on('connect', () => {
      this._log.info(`Create redis cache successfully with ${config.host} ${config.port}`);
    });
  }

  log(message, logName) {
    return this._log[logName](`Redis cache: ${typeof message === 'string' ? message : message.toString()}`);
  }

  add(key, value) {
    return new BPromise((resolve, reject) => {
      return this._redisClient.getAsync(key).then(reply => {
        if (reply) {
          this.log(`key ${key} exists`, 'error');
          return resolve(JSON.parse(reply));
        }
        let prom;
        value = typeof value === 'string' ? value : JSON.stringify(value);
        if (this.expire)
          prom = this._redisClient.setAsync(key, value, 'EX', this.expire);
        else
          prom = this._redisClient.setAsync(key, value);
        return prom.then(reply => {
          this.log(`add key ${key} successfully`, 'info');
          return resolve(reply);
        }).catch(err => {
          this.log(err, 'error');
          return reject(err);
        });

      });
    });
  }

  get(key) {
    return new BPromise((resolve, reject) => {
      return this._redisClient.getAsync(key).then(reply => {
        this.log(`get with key ${key} successfully`, 'info');
        return resolve(JSON.parse(reply));
      }).catch(err => {
        this.log(err, 'error');
        return reject(err);
      });
    });
  }

  del(key) {
    return new BPromise((resolve, reject) => {
      return this._redisClient.delAsync(key).then(reply => {
        return resolve(reply);
      }).catch(err => {
        this.log(err, 'error');
        return reject(err);
      });
    });
  }

  clearCache() {
    return new BPromise((resolve, reject) => {
      return this._redisClient.flushallAsync().then(reply => {
        this.log('delete all key successfully', 'info');
        return resolve(reply);
      }).catch(err => {
        this.log(err, 'error');
      });
    });
  }

}

module.exports = Redis;