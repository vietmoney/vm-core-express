const path = require('path');

class LogHelper {

  static checkLog(src) {
    const typeFile = 'jpg,png,gif,txt',
      extnameCurrent = path.extname(src).replace('.', ''),
      file = `['${typeFile.split(',').join(`','`)}']`;
    if (extnameCurrent.trim() === '') {
      return true;
    }
    if (file.indexOf(extnameCurrent) >= 0) {
      return false;
    }
    return true;
  }

}
module.exports = LogHelper;