/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-11-28 10:44:24 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2017-11-28 11:29:46
 */
class Render {

  static render(title) {
    return `
    <!DOCTYPE html>
    <html lang="vi">
        <head>
            <meta name="robots" content="NOINDEX,NOFOLLOW"/>
            <link rel="icon" href="/doc-socket/favicon.ico">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
            <title>${title || 'Documents for socket (Created by nhutdev(Tran Van Nhut))'}</title></title>
            <link rel="stylesheet" href="/doc-socket/style.css" />
        </head>
        <body>
            <div id="root"></div>
            <script type="application/javascript" src="/doc-socket/bundle.js"></script>
        </body>
    </html>`;
  }

}
module.exports = Render;