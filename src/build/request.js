const BPromise = require('bluebird');
const path = require('path');
const fs = BPromise.promisifyAll(require('fs'));
const express = require('express');
const JWT = require('../../lib/jwt');
const Hoek = require('hoek');
const OAuth = require('../../lib/oauth');
const JWTSchemas = require('helpers').Schemas.oAuth;

class BuildRequest {
  static buildRoutes(requests, log) {
    let routePath = 'routes',
      self = this;

    if (this._config && this._config.path && this._config.path.route) {
      routePath = this._config.path.route;
    }

    return new BPromise((resolve, reject) => {
      const routesPath = path.join(process.cwd(), routePath),
        handleMiddleware = (middleware, item) => {
          if (Array.isArray(middleware)) {
            middleware.unshift(item);
            return middleware;
          }
          return [item, middleware];
        };
      self._routes = {};
      self._genRouteSwagger = {};
      return fs.exists(routesPath, exist => {
        if (!exist) return resolve(self._routes);
        log.info('Building routes ...');
        return fs.readdirAsync(routesPath).then(folders => {
          folders.forEach((fd, i) => {
            let routeCurrent = [],
              router = express.Router(),
              nameRoute = fd.replace(/-/g, x => {
                return x.replace('-', '/');
              }),
              handle = {
                userJWT: requests[nameRoute]
                  ? requests[nameRoute].userJWT
                  : null,
                permission: requests[nameRoute]
                  ? requests[nameRoute].permission
                  : null,
                userBearer: requests[nameRoute]
                  ? requests[nameRoute].userBearer
                  : null,
                customAuth: requests[nameRoute]
                  ? requests[nameRoute].customAuth
                  : null
              };

            if (this._config.jsonWebToken)
              Hoek.assert(
                handle.userJWT,
                'If i active JWT,class handle request would have function userJWT'
              );

            fs.readdirAsync(`${routePath}/${fd}`).then(files => {
              files.forEach((b, y) => {
                routeCurrent = routeCurrent.concat(
                  require(`${routesPath}/${fd}/${b}`)
                );
                if (files.length === y + 1) {
                  routeCurrent.forEach(e => {
                    e.method = e.method.toLowerCase();
                    const controllerClass = e.controller;
                    let controller = controllerClass.middleware(e.middleware);
                    Hoek.assert(
                      controller,
                      `${
                        controllerClass.constructor.name
                      } not exist middleware ${
                        e.middleware
                      } at route have url ${e.url}`
                    );

                    if (e.request && e.request.auth) {
                      Hoek.assert(
                        e.request.headers,
                        `Custom auth cannot null headers at url ${e.url}`
                      );
                      Hoek.assert(
                        e.request.auth === 'custom',
                        'Value auth in valid "custom"'
                      );
                      Hoek.assert(
                        typeof handle.customAuth === 'function',
                        'Custom auth must exist function customAuth for request'
                      );

                      const handleJWT = new JWT();

                      e.middleware = handleMiddleware(
                        e.middleware,
                        handleJWT.customAuth.bind(handleJWT, handle.customAuth)
                      );
                      e.middleware = handleMiddleware(
                        e.middleware,
                        handleJWT.validateHeaderCustom.bind(
                          handleJWT,
                          e.request.headers
                        )
                      );
                    } else if (this._config.jsonWebToken) {
                      const handleJWT = new JWT(
                        handle.userJWT,
                        handle.permission
                      );

                      e.middleware = handleMiddleware(
                        e.middleware,
                        handleJWT.authorized.bind(handleJWT)
                      );

                      if (e.request && e.request.authorized) {
                        e.middleware = handleMiddleware(
                          e.middleware,
                          handleJWT.validateHeaderBearer.bind(handleJWT)
                        );
                        e.request.headers = JWTSchemas.headerBearer;
                      } else if (e.request && e.request.merge) {
                        e.middleware = handleMiddleware(
                          e.middleware,
                          handleJWT.validateHeaderMerge.bind(handleJWT)
                        );
                        e.request.headers = JWTSchemas.headerMerge;
                      } else {
                        e.middleware = handleMiddleware(
                          e.middleware,
                          handleJWT.validateHeaderBasic.bind(handleJWT)
                        );
                        e.request = e.request || {};
                        e.request.headers = JWTSchemas.headerBasic;
                      }
                    } else if (this._config.oAuth) {
                      const handleJWT = new JWT();

                      if (e.request && e.request.authorized) {
                        e.middleware = handleMiddleware(
                          e.middleware,
                          OAuth.bearer.bind(
                            OAuth,
                            false,
                            handle.userBearer,
                            handle.permission
                          )
                        );
                        e.middleware = handleMiddleware(
                          e.middleware,
                          handleJWT.validateHeaderBearer.bind(handleJWT)
                        );
                        e.request.headers = JWTSchemas.headerBearer;
                      } else if (e.request && e.request.merge) {
                        e.middleware = handleMiddleware(
                          e.middleware,
                          OAuth.merge.bind(
                            OAuth,
                            handle.userBearer,
                            handle.permission
                          )
                        );
                        e.middleware = handleMiddleware(
                          e.middleware,
                          handleJWT.validateHeaderMerge.bind(handleJWT)
                        );
                        e.request.headers = JWTSchemas.headerMerge;
                      } else {
                        e.middleware = handleMiddleware(
                          e.middleware,
                          OAuth.basic.bind(OAuth)
                        );
                        e.middleware = handleMiddleware(
                          e.middleware,
                          handleJWT.validateHeaderBasic.bind(handleJWT)
                        );
                        e.request = e.request || {};
                        e.request.headers = JWTSchemas.headerBasic;
                      }
                    } else {
                      if (e.request) {
                        if (e.request.authorized === true) {
                          e.middleware = handleMiddleware(
                            e.middleware,
                            'authorized'
                          );
                          if (handle.permission) {
                            e.middleware = handleMiddleware(
                              e.middleware,
                              controllerClass.handlePermission.bind(
                                controllerClass,
                                handle.permission
                              )
                            );
                          }
                        }
                        if (e.request.unauthorized === true)
                          e.middleware = handleMiddleware(
                            e.middleware,
                            'unauthorized'
                          );
                      }
                    }

                    if (e.request) {
                      let schema = e.request.schema || {};
                      if (e.request.paging)
                        schema.query = Object.assign(
                          schema.query ? schema.query : {},
                          e.request.paging.query || {}
                        );
                      controller = controllerClass.middleware(
                        e.middleware,
                        schema
                      );
                    }
                    controller = handleMiddleware(
                      controller,
                      this.beforeRoute.bind(this)
                    );
                    router.route(e.url)[e.method](controller);
                  });

                  if (this._config.swagger) {
                    if (/api/g.test(nameRoute)) {
                      self._genRouteSwagger[nameRoute] = routeCurrent;
                    }
                  }

                  self._routes[nameRoute] = router;
                  if (folders.length === i + 1) {
                    log.info('Building routes successfully');
                    return resolve(self._routes);
                  }
                }
              });
            });
          });
        });
      });
    }).catch(e => {
      throw e;
    });
  }

  static buildHandleRequest(log) {
    let requestPath = 'request',
      self = this;

    if (this._config && this._config.path && this._config.path.request) {
      requestPath = this._config.path.request;
    }

    return new BPromise((resolve, reject) => {
      let requestPaths = path.join(process.cwd(), requestPath);
      self._request = {};
      return fs.exists(requestPaths, exist => {
        if (!exist) return resolve(self._request);
        log.info('Building handle request ...');
        fs.readdirAsync(requestPaths).then(files => {
          if (files.length === 0) {
            return resolve(self._request);
          }
          files = files.filter(e => {
            if (path.extname(e) === '') {
              return false;
            }
            return true;
          });
          files.forEach((e, i) => {
            let pathCurrent = path.join(requestPaths, e),
              request = require(pathCurrent),
              nameRequestRoute = e
                .replace(path.extname(e), '')
                .replace(/-/g, x => {
                  return x.replace('-', '/');
                });

            const handleRequestCurrent = new request(),
              objRequest = {},
              funcAll = Object.getOwnPropertyNames(
                Object.getPrototypeOf(handleRequestCurrent)
              );
            funcAll.forEach(e => {
              if (e !== 'constructor') {
                objRequest[e] = handleRequestCurrent[e];
              }
            });
            if (objRequest.notFound) {
              if (!self._notFound) self._notFound = {};
              self._notFound[nameRequestRoute] = objRequest.notFound;
              delete objRequest.notFound;
            }
            if (Object.keys(objRequest).length > 0)
              self._request[nameRequestRoute] = objRequest;
            if (files.length === i + 1) {
              log.info('Building request successfully');
              return resolve(self._request);
            }
          });
        });
      });
    }).catch(e => {
      throw e;
    });
  }
}

module.exports = BuildRequest;
