const Request = require('request-promise');
const BPromise = require('bluebird');

class Merge {
  static requestCommon(cfg) {
    return new BPromise((resolve, reject) => {
      const { uri, method, body } = cfg,
        baseUrl = 'https://api.getpostman.com',
        defaultCfg = {
          uri: uri,
          baseUrl: baseUrl,
          method: method,
          headers: {
            'X-Api-Key': '616ac880d8e64ec68f1a017e41bcf669'
          },
          json: true
        };

      if (body) defaultCfg.body = body;
      console.log(`Loading api ${baseUrl}${uri}`);
      return Request(defaultCfg)
        .then(data => {
          console.log('Load completeted');
          return resolve(data);
        })
        .catch(err => {
          return reject(err);
        });
    });
  }

  static async getAll() {
    return await Merge.requestCommon({
      uri: '/collections',
      method: 'GET'
    });
  }

  static async getOne(uid) {
    return await Merge.requestCommon({
      uri: `/collections/${uid}`,
      method: 'GET'
    });
  }

  static async update(uid, body) {
    return await Merge.requestCommon({
      uri: `/collections/${uid}`,
      method: 'PUT',
      body: body
    });
  }

  static async upsert(uid, body) {
    if (uid)
      await Merge.requestCommon({
        uri: `/collections/${uid}`,
        method: 'DELETE',
        body: body
      });
    return await Merge.requestCommon({
      uri: '/collections',
      method: 'POST',
      body: {
        collection: body
      }
    });
  }
}

module.exports = Merge;
