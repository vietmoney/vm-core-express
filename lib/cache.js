const config = require('config');
const Redis = require('../src/cache/redis');
const log = require('./log');

let cache;
if (config.cache && config.cache.redis) {
  cache = new Redis(config.cache.redis, log);
}

module.exports = cache;