/**
 * @Author: Tran Van Nhut (nhutdev)
 * @Date: 2017-05-30 09:28:03
 * @Email:  tranvannhut4495@gmail.com
 * @Last modified by:   nhutdev
 * @Last modified time: 2017-05-30 09:28:03
 */

const Hoek = require('hoek');
const error = require('../src/error');
const pluralize = require('pluralize');
const helpers = require('helpers');
const config = require('config');
const BPromise = require('bluebird');

class BaseController {
  constructor(options) {
    if (options) {
      Hoek.assert(
        options.storeName,
        `Controller ${this.constructor} cannot undefined store class`
      );
      this._storeClass = options.storeName;
      this._nameStore = options.storeName.toLowerCase();
      this._keySingular = pluralize.singular(this._nameStore);
      this._keyPlural = pluralize.plural(this._nameStore);
      this._owner = options.ownerForm;
    }
  }

  getStore(req) {
    return req.getStore(this._storeClass);
  }

  authorized(req, res, next) {
    const keyUser = config.keyUser || 'user';
    req.commonData = req.commonData || {};
    if (req.commonData[keyUser]) {
      return next();
    }
    return req.replyError(
      req,
      res,
      {
        code: '504',
        source: 'require authorized'
      },
      {
        statusCode: '401'
      }
    );
  }

  unauthorized(req, res, next) {
    const keyUser = config.keyUser || 'user';
    if (!req.commonData[keyUser]) {
      return next();
    }
    return req.replyError(req, res, {
      code: '500',
      source: 'require unauthorized'
    });
  }

  handlePermission(handle, req, res, next) {
    return handle(req, res)
      .then(() => {
        return next();
      })
      .catch(err => {
        return req.replyError(req, res, err);
      });
  }

  get(req, res) {
    const id = req.params.id,
      store = this.getStore(req),
      primaryKey = store.model.primaryKey || 'id';
    return store
      .filter({
        [primaryKey]: id
      })
      .then(result => {
        let data = {
          [this._keySingular]: result
        };

        return req.reply(req, res, data, {
          message: `Get ${this._keySingular} successfully`
        });
      })
      .catch(err => {
        return req.replyError(req, res, err);
      });
  }

  delete(req, res) {
    const id = req.params.id,
      store = this.getStore(req),
      primaryKey = store.model.primaryKey || 'id';
    return store
      .delete(
        {
          [primaryKey]: id
        },
        {
          checkNull: true
        }
      )
      .then(() => {
        return req.reply(
          req,
          res,
          {},
          {
            message: `Delete one ${this._keySingular} successfully`
          }
        );
      })
      .catch(err => {
        return req.replyError(req, res, err);
      });
  }

  list(req, res) {
    const store = this.getStore(req),
      query = helpers.Json.cloneDeep(req.query),
      model = store.model;
    let keyOrder = model.primaryKey;
    const parseValue = (key, value) => {
      if (model[key] === '')
        return {
          like: value
        };
      return value;
    };
    Object.keys(query).forEach(e => {
      query[e] = parseValue(e, query[e]);
    });

    if (typeof model.createdAt !== 'undefined') keyOrder = 'createdAt';

    ['pageNumber', 'pageSize'].forEach(e => {
      if (query[e]) delete query[e];
    });

    return store
      .filterPagination(query, {
        paging: helpers.Request.parseParamPaging(req),
        order: `-${keyOrder}`
      })
      .then(result => {
        return req.reply(req, res, result, {
          message: `Get list ${this._keyPlural} successfully`
        });
      })
      .catch(err => {
        return req.replyError(req, res, err);
      });
  }

  parseFormOwner(req, res, next, form, opts) {
    return new BPromise((resolve, reject) => {
      opts = opts || {};
      const keyUser = config.keyUser || 'user',
        user = req.commonData ? req.commonData[keyUser] || null : null;
      if (this.beforeForm) {
        form = this.beforeForm(form, opts) || form;
      }
      if (!this._owner) return resolve(form);
      if (this._owner && !user) {
        req.log.error(
          `Route "${req.path}" is active owner, but authorized is not active`
        );
        return this.authorized(req, res, next);
      }
      if (this._owner && user) {
        const owner = this._owner || {},
          valueOwner = user[owner.keyFormValue || 'id'];
        form[owner.keyFormUpdatedBy || 'updatedBy'] = valueOwner;
        if (!opts.update)
          form[owner.keyFormCreatedBy || 'createdBy'] = valueOwner;
      }
      return resolve(form);
    });
  }

  insert(req, res, next) {
    const store = this.getStore(req),
      form = req.body;
    return this.parseFormOwner(req, res, next, form)
      .then(form => {
        return store
          .insert(form, {
            returnData: true
          })
          .then(result => {
            let data = {
              [this._keySingular]: result
            };
            return req.reply(req, res, data, {
              message: `Insert ${this._keySingular} successfully`
            });
          })
          .catch(err => {
            return req.replyError(req, res, err);
          });
      })
      .catch(err => {
        return req.replyError(req, res, err);
      });
  }

  update(req, res, next) {
    const store = this.getStore(req),
      id = req.params.id,
      form = req.body,
      primaryKey = store.model.primaryKey;
    return this.parseFormOwner(req, res, next, form, {
      update: true
    })
      .then(form => {
        return store
          .update(
            form,
            {
              [primaryKey]: id
            },
            {},
            {
              returnData: true
            }
          )
          .then(result => {
            let data = {
              [this._keySingular]: result
            };

            return req.reply(req, res, data, {
              message: `Update ${this._keySingular} successfully`
            });
          })
          .catch(err => {
            return req.replyError(req, res, err);
          });
      })
      .catch(err => {
        return req.replyError(req, res, err);
      });
  }

  validate(schema, opts, req, res, next) {
    let errors = [],
      checkInputNull = schema ? (schema.files ? false : true) : true;
    if (opts && opts.paging) {
      if (schema && schema.query) {
        schema.query = Object.assign(schema.query, opts.paging.query);
      } else {
        schema.query = opts.paging.query;
      }
    }
    const key = Object.keys(schema);

    key.forEach(e => {
      const data = req[e],
        dataError = error.validate(schema[e], data),
        listInorgeEmptyObject = ['query', 'params'];

      if (dataError.length > 0) {
        errors = errors.concat(dataError);
      } else if (
        dataError.length === 0 &&
        checkInputNull &&
        !listInorgeEmptyObject.includes(e)
      ) {
        let currentError;
        if (typeof data === 'object' && helpers.Json.checkEmptyObject(data)) {
          currentError = error.translate({
            code: '514',
            source: 'validate inside core'
          });
        } else if (Array.isArray(data) && data.length === 0) {
          currentError = error.translate({
            code: '515',
            source: 'validate inside core'
          });
        }
        if (currentError) errors = errors.concat(currentError);
      }
    });

    if (errors.length > 0) {
      return req.replyError(req, res, errors);
    }
    const parseValueRequest = key => {
      if (schema[key] && !helpers.Json.checkEmptyObject(req[key])) {
        Object.keys(req[key]).forEach(e => {
          if (helpers.Number.isNumber(req[key][e])) {
            req[key][e] = parseFloat(req[key][e]);
          }
        });
      }
    };
    parseValueRequest('query');
    parseValueRequest('params');
    return next();
  }

  middleware(middleware, schema, opts) {
    let listMiddleware = [];
    const funcSchema = this.validate.bind(this, schema, opts);
    if (schema) {
      Hoek.assert(
        schema,
        'validate route cannot null schema in params middleware'
      );
      listMiddleware.push(funcSchema);
    }
    if (typeof middleware !== 'string') {
      middleware.forEach(e => {
        if (typeof e === 'string') {
          if (typeof this[e] === 'function') {
            listMiddleware.push(this[e].bind(this));
          }
        } else {
          if (typeof e === 'function') {
            listMiddleware.push(e);
          }
        }
      });
      /**
       * @param 1 : to,
       * @param 2: from
       listMiddleware.splice(param1, 0, listMiddleware.splice(param2, 1)[0]);
       */
      listMiddleware = listMiddleware.filter(e => {
        if (e.name == 'bound validate') {
          return false;
        }
        return true;
      });

      if (schema)
        listMiddleware.splice(listMiddleware.length - 1, 0, funcSchema);
      return listMiddleware;
    }
    if (typeof this[middleware] === 'function') {
      listMiddleware.push(this[middleware].bind(this));
      return listMiddleware;
    }
  }
}

module.exports = BaseController;
