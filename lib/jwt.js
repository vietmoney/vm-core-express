const config = require('config');
const jwt = require('jsonwebtoken');
const errorHelpers = require('../src/error');
const helpers = require('helpers');
const schema = helpers.Schemas.oAuth;
const Hoek = require('hoek');
const BPromise = require('bluebird');

class JWT {
  constructor(handle, permission) {
    this.userJWT = handle;
    this.permission = permission;
  }

  validateHeaderBearer(req, res, next) {
    return this.validateHeader(
      req,
      {
        authorized: true
      },
      res,
      next
    );
  }

  validateHeaderMerge(req, res, next) {
    return this.validateHeader(
      req,
      {
        merge: true
      },
      res,
      next
    );
  }

  validateHeaderBasic(req, res, next) {
    return this.validateHeader(req, {}, res, next);
  }

  validateTokenBasic(src) {
    const token = new Buffer(src, 'base64').toString('ascii').split('.'),
      clientSecret = process.env.clientSecret,
      clientId = process.env.clientId;
    if (token[0] === clientSecret && token[1] === clientId) return true;
    return false;
  }

  validateHeaderCustom(schema, req, res, next) {
    const headers = {
      authorization: req.headers.authorization
    },
      errors = errorHelpers.validate(schema, headers);
    if (errors.length > 0) return req.replyError(req, res, errors);
    return next();
  }

  async customAuth(customAuth, req, res, next) {
    try {
      const dataCredentials = await customAuth(req, res);
      req.commonData.credentials = dataCredentials;
      return next();
    } catch (error) {
      return req.replyError(req, res, error);
    }
  }


  validateHeader(req, route, res, next) {
    route = route || {};
    const headers = {
      authorization: req.headers.authorization
    };
    let schemaCurrent = schema.headerBasic;
    if (route.authorized) {
      schemaCurrent = schema.headerBearer;
    }
    if (route.merge) {
      const errorForMerge = errorHelpers.validate(schema.headerMerge, headers);
      if (errorForMerge.length > 0)
        return req.replyError(req, res, errorForMerge);
      return next();
    }
    const errors = errorHelpers.validate(schemaCurrent, headers);
    if (errors.length == 0) return next();
    return req.replyError(req, res, errors);
  }

  static sign(data, opts) {
    opts = opts || {};
    Hoek.assert(data.id, '[Sign JWT] data need exist key "id"');

    return jwt.sign(data, opts.secret, {
      expiresIn: opts.expiresIn || config.jwt.expiresIn,
      issuer: opts.issuer
    });
  }

  static verifyToken(token) {
    return jwt.verify(token, config.jwt.secret, (err, decode) => {
      if (err) {
        if (err.name === 'TokenExpiredError')
          return BPromise.reject({
            code: '511',
            source: 'unauthorized'
          });
        return BPromise.reject({
          code: '509',
          source: 'unauthorized'
        });
      }
      return BPromise.resolve(decode);
    });
  }

  authorized(req, res, next) {
    const authorization = req.headers.authorization.split(' '),
      type = authorization[0],
      token = authorization[1];

    if (type === 'Basic') {
      const validate = this.validateTokenBasic(token);
      if (validate) return next();
      return req.replyError(
        req,
        res,
        {
          code: '509',
          source: 'unauthorized'
        },
        {
          statusCode: '401'
        }
      );
    } else {
      if (req.headers['authorization']) {
        return this.userJWT(req, res)
          .then(user => {
            req.commonData = req.commonData || {};
            req.commonData[config.keyUser || 'user'] = user;
            if (this.permission) {
              return this.permission(req, res)
                .then(() => {
                  return next();
                })
                .catch(err => {
                  return req.replyError(req, res, err, {
                    statusCode: '403'
                  });
                });
            }
            return next();
          })
          .catch(err => {
            return req.replyError(req, res, err);
          });
      } else {
        return req.replyError(req, res, {
          statusCode: '401'
        });
      }
    }
  }

}

module.exports = JWT;
